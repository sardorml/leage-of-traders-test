import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    beforeEnter: (to, from, next) => {
      if(localStorage.getItem("token")){
        return next({
          name: "profile"
        })
      }
      next()
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/Profile'),
    beforeEnter: (to, from, next) => {
      if(!localStorage.getItem("token")){
        return next({
          name: "login"
        })
      }
      next()
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
