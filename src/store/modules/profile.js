import Axios from "axios"

const state = () => ({
  currency: {
    symbol: "USD",
    value: 1
  },
  profileInfo: ''
})

// getters
const getters = {
    getCurrency: (state) => state.currency,
    getProfileInfo: (state) => state.profileInfo
}

// actions
const actions = {
  async fetchProfileInfo({commit},id) {
    try {
      let response = await Axios.get(`profile/${id}`)
      commit("setProfileInfo",response.data)
    } catch(e) {
      console.log(e.response.data.Message)
    }
  }
}

// mutations
const mutations = {
    setCurrency(state,currency){
        state.currency = currency
        localStorage.setItem('currency',JSON.stringify(currency))
    },
    setProfileInfo(state,profile){
        state.profileInfo = profile
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}