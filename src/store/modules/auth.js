import Axios from "axios"

const state = () => ({
  userId: '',
  token: ''
})

// getters
const getters = {
    getUserId: (state) => localStorage.getItem("userId") || state.userId,
    getToken: (state) => localStorage.getItem("token") || state.token
}

// actions
const actions = {
  async authenticate({commit},user){
    try {
      console.log(user)
      let response = await Axios.post('login/email',user)
      commit('setUserId',response.data.user_id)
      commit('setToken',response.data.access_token)
      return 200      
    } catch(e) {
      return e.response.data.Message
    }
  }
}

// mutations
const mutations = {
    setUserId(state,userId){
        state.userId = userId
        localStorage.setItem("userId",userId)
    },
    setToken(state,token){
        state.token = token
        localStorage.setItem("token",token)
    },
    // async upadateUserInfo(){
    //     try {
    //         Axios.get()
    //     } catch (e) {
    //         console.log(e)
    //     }
    // }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}